package ru.ardecs.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import ru.ardecs.web.domain.Booking;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Locale;

@Service
public class EmailService {
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;

    public void sendMail(Booking booking, String recipientEmail, Locale locale) throws MessagingException {

        String intervalForSend = booking.getIntervalByExistsTimeIntervalId().getTimeInterval();
        Context ctx = new Context(locale);
        ctx.setVariable("firstName", booking.getDoctor().getFirstName());
        ctx.setVariable("lastName", booking.getDoctor().getLastName());
        ctx.setVariable("timeInterval", intervalForSend);
        ctx.setVariable("date", booking.getDate());

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        message.setSubject("Талон");
        message.setFrom("thymeleaf@example.com");
        message.setTo(recipientEmail);
        message.setText(templateEngine.process("ticketEmail", ctx), true);
        mailSender.send(mimeMessage);
    }
}
