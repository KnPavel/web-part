package ru.ardecs.web.domain.request;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by knpavel on 24.07.17.
 */
public class DoctorRequestModel {
    private int doctorId;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }
}
