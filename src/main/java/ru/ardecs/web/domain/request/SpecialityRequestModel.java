package ru.ardecs.web.domain.request;

/**
 * Created by knpavel on 21.07.17.
 */
public class SpecialityRequestModel {
    private int specialityId;

    public int getSpecialityId() {
        return specialityId;
    }

    public void setSpecialityId(int specialityId) {
        this.specialityId = specialityId;
    }
}