package ru.ardecs.web.domain;

/**
 * Created by knpavel on 14.07.17.
 */

public class MedicalSpeciality {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
