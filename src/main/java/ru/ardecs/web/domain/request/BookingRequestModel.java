package ru.ardecs.web.domain.request;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by knpavel on 26.07.17.
 */
public class BookingRequestModel {
    private int timeIntervalId;
    private int doctorId;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

    public int getTimeIntervalId() {
        return timeIntervalId;
    }

    public void setTimeIntervalId(int timeIntervalId) {
        this.timeIntervalId = timeIntervalId;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
