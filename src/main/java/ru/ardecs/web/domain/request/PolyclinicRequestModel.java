package ru.ardecs.web.domain.request;

/**
 * Created by knpavel on 24.07.17.
 */
public class PolyclinicRequestModel {
    private int polyclinicId;

    public int getPolyclinicId() {
        return polyclinicId;
    }

    public void setPolyclinicId(int polyclinicId) {
        this.polyclinicId = polyclinicId;
    }
}
