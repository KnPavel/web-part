package ru.ardecs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ardecs.web.domain.request.DoctorRequestModel;
import ru.ardecs.web.domain.request.PolyclinicRequestModel;
import ru.ardecs.web.domain.request.SpecialityRequestModel;
import ru.ardecs.web.restfull.ClientRequestServices;

/**
 * Created by knpavel on 19.07.17.
 */
@Controller
@RequestMapping("/doctor")
public class DoctorController {

    @Autowired
    private ClientRequestServices clientRequestServices;

    @GetMapping
    public String getDoctorPage(SpecialityRequestModel specialityRequestModel,
                                PolyclinicRequestModel polyclinicRequestModel, Model model) {
        model.addAttribute("doctor", clientRequestServices.getAllDoctorBySpecialityIdAndPolyclinicId(
            specialityRequestModel.getSpecialityId(), polyclinicRequestModel.getPolyclinicId()));
        model.addAttribute("doctorRequestModel", new DoctorRequestModel());
        return "doctor";
    }
}
