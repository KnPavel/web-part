package ru.ardecs.web.restfull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.ardecs.web.domain.Booking;
import ru.ardecs.web.domain.Doctor;
import ru.ardecs.web.domain.MedicalSpeciality;
import ru.ardecs.web.domain.Polyclinic;
import ru.ardecs.web.domain.dto.BookingStatistics;
import ru.ardecs.web.domain.dto.TransferBookingData;

import java.util.Date;

/**
 * Created by knpavel on 18.07.17.
 */

@Component
public class ClientRequestServices {
	
    @Autowired
    private RestTemplate restTemplate;

    @Value("${speciality.api.url}")
    private String specialityUrl;

    @Value("${polyclinic.api.url}")
    private String polyclinicBySpecialityIdUrl;

    @Value("${doctor.api.url}")
    private String doctorUrl;

    @Value("${one-doctor.api.url}")
    private String oneDoctorUrl;

    @Value("${booking.api.url}")
    private String bookingUrl;

    @Value("${one-booking.api.url}")
    private String oneBookingUrl;

    @Value("${patient.api.url}")
    private String patientUrl;

    @Value("${email.api.url}")
    private String emailUrl;

    @Value("${statistic.api.url}")
    private String statisticUrl;

    @Value("${years.api.url}")
    private String yearsUrl;

    public MedicalSpeciality[] getAllSpeciality() {
        return restTemplate.getForObject(specialityUrl, MedicalSpeciality[].class);
    }

    public Polyclinic[] getPolyclinicBySpecialityId(Integer id) {
        return restTemplate.getForObject(polyclinicBySpecialityIdUrl, Polyclinic[].class, id);
    }

    public Doctor[] getAllDoctorBySpecialityIdAndPolyclinicId(Integer specialityId, Integer polyclinicId) {
        return restTemplate.getForObject(doctorUrl, Doctor[].class, specialityId, polyclinicId);
    }

    public Booking[] getBookingByDateAndDoctorId(Integer id, Date date) {
        return restTemplate.postForObject(bookingUrl, date, Booking[].class, id);
    }

    public Booking getBookingById(Integer id) {
        return restTemplate.getForObject(oneBookingUrl, Booking.class, id);
    }

    public Booking sendPatientData(TransferBookingData bookingData) {
        return restTemplate.postForObject(patientUrl, bookingData, Booking.class);
    }

    public Doctor getDoctorById(Integer id) {
        return restTemplate.getForObject(oneDoctorUrl, Doctor.class, id);
    }

    public BookingStatistics[] getStatisticByYear(Integer year) {
        return restTemplate.getForObject(statisticUrl, BookingStatistics[].class, year);
    }

    public Integer[] getYears() {
        return restTemplate.getForObject(yearsUrl, Integer[].class);
    }
}
