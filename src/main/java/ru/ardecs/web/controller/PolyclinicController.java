package ru.ardecs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ardecs.web.domain.request.PolyclinicRequestModel;
import ru.ardecs.web.domain.request.SpecialityRequestModel;
import ru.ardecs.web.restfull.ClientRequestServices;

/**
 * Created by knpavel on 18.07.17.
 */
@Controller
@RequestMapping("/polyclinic")
public class PolyclinicController {

    @Autowired
    private ClientRequestServices clientRequestServices;

    @GetMapping
    public String getPolyclinicPage(SpecialityRequestModel specialityRequestModel, Model model) {
        model.addAttribute("polyclinic", clientRequestServices.getPolyclinicBySpecialityId(
            specialityRequestModel.getSpecialityId()));
        model.addAttribute("polyclinicRequestModel", new PolyclinicRequestModel());
        model.addAttribute("specId", specialityRequestModel.getSpecialityId());
        return "polyclinic";
    }
}
