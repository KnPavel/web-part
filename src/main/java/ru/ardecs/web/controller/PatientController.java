package ru.ardecs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.ardecs.web.domain.Booking;
import ru.ardecs.web.domain.dto.TransferBookingData;
import ru.ardecs.web.domain.request.BookingRequestModel;
import ru.ardecs.web.restfull.ClientRequestServices;
import ru.ardecs.web.service.EmailService;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.Locale;

/**
 * Created by knpavel on 26.07.17.
 */
@Controller
@RequestMapping("/booking/patient")
public class PatientController {

    @Autowired
    private ClientRequestServices clientRequestServices;

    @Autowired
    private EmailService emailService;

    @PostMapping
    public String getPatientPage(BookingRequestModel requestModel, Model model) {
        model.addAttribute("dataForTransfer", new TransferBookingData());
        model.addAttribute("booking", new Booking());
        model.addAttribute("docId", requestModel.getDoctorId());
        model.addAttribute("timeId", requestModel.getTimeIntervalId());
        model.addAttribute("selectDate", requestModel.getDate());
        return "patient";
    }

    @PostMapping("/add")
    public String sendPatientData(@Valid TransferBookingData dataForTransfer,
                                  BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", true);
            return "redirect:/";
        }
        Booking booking = clientRequestServices.sendPatientData(dataForTransfer);
        redirectAttributes.addFlashAttribute("id", booking.getId());
        return "redirect:ticket";
    }

    @GetMapping("/ticket")
    public String showTicket(@ModelAttribute("id") Integer id, Model model) {
        Booking booking = clientRequestServices.getBookingById(id);
        model.addAttribute("doctor", booking.getDoctor());
        model.addAttribute("interval", booking.getIntervalByExistsTimeIntervalId());
        model.addAttribute("id", id);
        model.addAttribute("date", booking.getDate());
        model.addAttribute("email", String.class);
        return "ticket";
    }

    @PostMapping("/send")
    public String sendMail(Integer id, String email, Model model) {
        Booking booking = clientRequestServices.getBookingById(id);
        model.addAttribute("id", booking.getId());
        try {
            emailService.sendMail(booking, email, Locale.ROOT);
            model.addAttribute("error", false);
        } catch (MessagingException e) {
            model.addAttribute("error", true);
        }
        return "fragments/fragments :: resultsList";
    }
}