package ru.ardecs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ardecs.web.domain.Booking;
import ru.ardecs.web.domain.Doctor;
import ru.ardecs.web.domain.Interval;
import ru.ardecs.web.domain.request.BookingRequestModel;
import ru.ardecs.web.domain.request.DoctorRequestModel;
import ru.ardecs.web.restfull.ClientRequestServices;

import java.util.List;

/**
 * Created by knpavel on 24.07.17.
 */
@Controller
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    private ClientRequestServices clientRequestServices;

    @GetMapping
    public String getBookingByDateAndDoctorId(BookingRequestModel requestModel,
                                              DoctorRequestModel doctorRequestModel, Model model) {
        Booking[] bookings = clientRequestServices.getBookingByDateAndDoctorId(doctorRequestModel.getDoctorId(),
                doctorRequestModel.getDate());
        Doctor doctor = clientRequestServices.getDoctorById(doctorRequestModel.getDoctorId());
        model.addAttribute("docId", doctorRequestModel.getDoctorId());
        model.addAttribute("date", doctorRequestModel.getDate());
        model.addAttribute("doc", doctor);
        model.addAttribute("bookingRequestModel", requestModel);
        model.addAttribute(doctorRequestModel);

        List<Interval> intervals = doctor.getIntervals();

        for (Interval interval :  intervals) {
            for (Booking booking : bookings) {
                if (interval.getId() == booking.getTimeIntervalId()) {
                    interval.setId(-1);
                    break;
                }
            }
        }

        model.addAttribute("interval", intervals);
        return "booking";
    }
}
