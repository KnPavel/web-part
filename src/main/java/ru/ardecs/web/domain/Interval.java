package ru.ardecs.web.domain;

/**
 * Created by knpavel on 24.07.17.
 */
public class Interval {

    private int id;
    private String timeInterval;

    public Interval() {

    }

    public Interval(int id, String timeInterval) {
        this.id = id;
        this.timeInterval = timeInterval;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(String timeInterval) {
        this.timeInterval = timeInterval;
    }

}
