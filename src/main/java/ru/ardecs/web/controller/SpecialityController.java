package ru.ardecs.web.controller;

import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.queries.users.UserField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.vkontakte.api.VKontakte;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ardecs.web.domain.request.SpecialityRequestModel;
import ru.ardecs.web.restfull.ClientRequestServices;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by knpavel on 18.07.17.
 */
@Controller
@RequestMapping({"/", "/speciality"})
public class SpecialityController {

    @Autowired
    private ClientRequestServices clientRequestServices;

    @Autowired
    private ConnectionRepository connectionRepository;

    @Value("${spring.social.vk.app-id}")
    private Integer appId;

    @Value("${spring.social.vk.app-secret}")
    private String secretId;

    @Value("${spring.social.vk.redirect-url}")
    private String redirectUrl;

    @GetMapping
    public String getSpecialityPage(HttpServletRequest request, Model model) {
        model.addAttribute(request.getSession());
        model.addAttribute("speciality", clientRequestServices.getAllSpeciality());
        model.addAttribute("specialityRequestModel", new SpecialityRequestModel());
        return "speciality";
    }

    @GetMapping("/vkontakte")
    public String signInVk(HttpServletRequest request) throws ApiException, ClientException {
        Connection<VKontakte> connection = connectionRepository.findPrimaryConnection(VKontakte.class);
        if (connection == null) {
            return "redirect:/signin/vkontakte";
        }
        VkApiClient vk = new VkApiClient(HttpTransportClient.getInstance());
        request.getSession().setAttribute("vkUser", vk.users().get(connection.getApi().getUserActor())
                .fields(UserField.SCREEN_NAME).execute().get(0));
        return "redirect:/speciality";
    }

    @GetMapping("/vkontakte/logout")
    public String logoutVk(HttpServletRequest request) {
        request.getSession().invalidate();
        return "redirect:/speciality";
    }

    @GetMapping("/login")
    private String loginPage() {
        return "login";
    }
}
