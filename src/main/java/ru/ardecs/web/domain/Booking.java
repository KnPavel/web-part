package ru.ardecs.web.domain;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by knpavel on 24.07.17.
 */
public class Booking {

    private int id;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;
    private int timeIntervalId;
    private Doctor doctor;
    private Patient patient;

    public Booking() {

    }

    public Booking(int id, Date date, int timeIntervalId, Doctor doctor, Patient patient) {
        this.id = id;
        this.date = date;
        this.timeIntervalId = timeIntervalId;
        this.doctor = doctor;
        this.patient = patient;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTimeIntervalId() {
        return timeIntervalId;
    }

    public void setTimeIntervalId(int timeIntervalId) {
        this.timeIntervalId = timeIntervalId;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Interval getIntervalByExistsTimeIntervalId() {
        for (Interval interval : this.doctor.getIntervals()) {
            if (interval.getId() == this.timeIntervalId) {
                return interval;
            }
        }
        return null;
    }
}
