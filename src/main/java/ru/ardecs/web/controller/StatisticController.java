package ru.ardecs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ardecs.web.domain.dto.BookingStatistics;
import ru.ardecs.web.restfull.ClientRequestServices;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/statistic")
public class StatisticController {

    @Autowired
    private ClientRequestServices clientRequestServices;
    private final String[] MONTHS = {"Январь", "Февраль", "Март", "Аперль", "Май", "Июнь", "Июль", "Август",
            "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};

    @GetMapping
    public String getStatisticPage(Model model) {
        model.addAttribute("years", clientRequestServices.getYears());
        return "statistic";
    }

    @GetMapping("/{year}")
    public String getStatisticByYear(@PathVariable("year") Integer year, Model model) {
        model.addAttribute("months", MONTHS);
        model.addAttribute("allSpeciality", clientRequestServices.getAllSpeciality());

        BookingStatistics[] statistic = clientRequestServices.getStatisticByYear(year);
        Map<String, Integer> map = new HashMap<>();
        for (BookingStatistics st : statistic) {
            map.put(st.getId() + "_" + st.getMonth(), st.getCount());
        }
        model.addAttribute("map", map);
        return "fragments/fragments :: statisticBlock";
    }
}